const itemDb = require('../Database/ItemDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Item = require('../Entities/Item');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const items = await itemDb.getItem();     
     if(items != null){
        console.log(' Items retrieved!');
        res.send(items);
     }else{
        console.log(' Items not retrieved');
        res.send(' Items not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get items');
         console.log('Exception: Cannot get items');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientItem = req.body;     
        console.log("The item",clientItem);
                     
        const result = await itemDb.addItem(clientItem);
        if (result == 1)
        {
            res.send("item Added!");
            console.log("item added successfully");
        }else{
            res.send("item not added!");
            console.log("item not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: item not added");
        res.send('Exception: item not added');
    }
})

router.put('/:id',async (req,res)=>{    
    try{   
        const JSONId = req.params;                                  
        const id = JSONId.id              
        const o_id = new ObjectId(id);  
        const clientItem = req.body; 

        const result = await itemDb.updateItem(clientItem,o_id);        
        if(result != null)
        {
            res.send('item updated');
            console.log('item updated');
        }else{
            console.log('Item entry not found');
            res.send('Item entry not found');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: item not updated');
        res.send('Exception: item not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const JSONId = req.params;                                
        const id = JSONId.id      
        const o_id = new ObjectId(id);  
        const result = await itemDb.deleteItem(o_id);        
        if(result == 1)
        {
            res.send('Item deleted successfully');
            console.log('Item deleted successfully');
        }else{
            console.log('Item not deleted');
            res.send('Item not deleted');
        }
    }catch(error){
        //console.error(error);
        console.log('Controller Exception: Item not deleted ');
        res.send('Exception: Item not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await itemDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Item found!');
        }else{
            res.send('Item not found!');
            console.log('Item not found!');
        }
    }catch(error)
    {
        console.error(error);
        console.log('Controller Exception: Cannot get item by id');
        res.send(' Exception: Cannot get item');
    }
})


router.get('/username/:username', async (req,res)=>{
    try{        
        const userNameObject = req.params;                
        const userName =  userNameObject.username;        
        const result = await itemDb.getByUsername(userName);        
        if(result != null)
        {
            res.send(result);
            console.log('Item found!');
        }else{
            res.send('Item not found!');
            console.log('Item not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get Item by username');
        res.send('Exception: Cannot get Item by username');
    }
})


module.exports = router;
