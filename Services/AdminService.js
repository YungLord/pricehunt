const adminDb = require('../Database/AdminDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Admin = require('../Entities/Admin');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const admins = await adminDb.getAdmin();     
     if(admins != null){
        console.log(' Admins retrieved!');
        res.send(admins);        
     }else{
        console.log(' Admins not retrieved');
        res.send(' Admins not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get admins');
         console.log('Exception: Cannot get admins');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientAdmin = req.body;                     
        const token = await adminDb.addAdmin(clientAdmin);
        if (token )
        {
            return res.status(201).send({ auth: true, token: token })
            console.log("admin added successfully");
        }else{
            return res.status(400).send({ auth: false, token: null })
            console.log("admin not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: admin not added");
        return res.send('Exception: admin not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const JSONId = req.params;                                  
        const id = JSONId.id              
        const o_id = new ObjectId(id);  
        const clientAdmin = req.body; 

        const result = await adminDb.updateAdmin(clientAdmin,o_id);        
        if(result != null)
        {
            res.send('admin updated');
            console.log('admin updated');
        }else{
            console.log('Admin entry not found');
            res.send('Admin entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: admin not updated');
        res.send('Exception: admin not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const JSONId = req.params;                                
        const id = JSONId.id      
        const o_id = new ObjectId(id);  
        const result = await adminDb.deleteAdmin(o_id);        
        if(result == 1)
        {
            res.send('Admin deleted successfully');
            console.log('Admin deleted successfully');
        }else{
            console.log('Admin not deleted');
            res.send('Admin not deleted');
        }
    }catch(error){
        //console.error(error);
        console.log('Controller Exception: Admin not deleted ');
        res.send('Exception: Admin not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await adminDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Admin found!');
        }else{
            res.send('Admin not found!');
            console.log('Admin not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get admin by id');
        res.send(' Exception: Cannot get admin');
    }
})

router.get('/username/:user', async (req,res)=>{
    try{        
        const adminNameObject = req.params;                
        const username =  adminNameObject.user;        
        const result = await adminDb.getByAdminname(username);        
        if(result != null)
        {
            res.send(result);
            console.log('Admin found!');
        }else{
            res.send('Admin not found!');
            console.log('Admin not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get Admin by username`');
        res.send('Exception: Cannot get Admin by username');
    }
})



router.post('/login',async (req,res)=>{
    try{
        const clientAdmin  = req.body;
        const username = clientAdmin.username;                     
        const serverAdmin = await adminDb.getByUsername(username)    
        console.log('The record',serverAdmin);
        
        if (serverAdmin == null)
        {
            console.log('Admin does not exist');
            res.send('Admin does not exist');
        }else{
            const  hash = serverAdmin.password;
            console.log(hash);
            
            const  plainTextPassword = clientAdmin.password;            
            const result = await bcrypt.compare(plainTextPassword,hash);            
            const secret = process.env.accessToken;
            const token = jwt.sign({username :clientAdmin.username },secret)
            if(result)
                {
                    console.log('Admin authenticated');
                    return res.status(201).send({auth: true, token: token});      
                }else{
                    console.log('Incorrect password');
                    return res.status(201).send({auth: false, token: null});  
                }
        }
        }catch(err){
            console.log('Controller Exception: cannot authenticate');
            console.error(err);
            return res.send('Cannot Authenticate');
        }
    })

module.exports = router;