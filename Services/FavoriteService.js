const favoriteDb = require('../Database/FavoriteDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Favorite = require('../Entities/Favorite');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const favorites = await favoriteDb.getFavorite();     
     if(favorites != null){
        console.log(' Favorites retrieved!');
        res.send(favorites);        
     }else{
        console.log(' Favorites not retrieved');
        res.send(' Favorites not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get favorites');
         console.log('Exception: Cannot get favorites');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientFavorite = req.body;     
        console.log("The favorite",clientFavorite);
                     
        const result = await favoriteDb.addFavorite(clientFavorite);
        if (result == 1)
        {
            res.send("favorite Added!");
            console.log("favorite added successfully");
        }else{
            res.send("favorite not added!");
            console.log("favorite not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: favorite not added");
        res.send('Exception: favorite not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const JSONId = req.params;                                  
        const id = JSONId.id              
        const o_id = new ObjectId(id);  
        const clientFavorite = req.body; 

        const result = await favoriteDb.updateFavorite(clientFavorite,o_id);        
        if(result != null)
        {
            res.send('favorite updated');
            console.log('favorite updated');
        }else{
            console.log('Favorite entry not found');
            res.send('Favorite entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: favorite not updated');
        res.send('Exception: favorite not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const JSONId = req.params;                                
        const id = JSONId.id      
        const o_id = new ObjectId(id);  
        const result = await favoriteDb.deleteFavorite(o_id);        
        if(result == 1)
        {
            res.send('Favorite deleted successfully');
            console.log('Favorite deleted successfully');
        }else{
            console.log('Favorite not deleted');
            res.send('Favorite not deleted');
        }
    }catch(error){
        //console.error(error);
        console.log('Controller Exception: Favorite not deleted ');
        res.send('Exception: Favorite not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await favoriteDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Favorite found!');
        }else{
            res.send('Favorite not found!');
            console.log('Favorite not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get favorite by id');
        res.send(' Exception: Cannot get favorite');
    }
})


router.get('/username/:username', async (req,res)=>{
    try{        
        const userNameObject = req.params;                
        const userName =  userNameObject.username;        
        const result = await favoriteDb.getByUsername(userName);        
        if(result != null)
        {
            res.send(result);
            console.log('Favorite found!');
        }else{
            res.send('Favorite not found!');
            console.log('Favorite not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get Favorite by username');
        res.send('Exception: Cannot get Favorite by username');
    }
})


module.exports = router;