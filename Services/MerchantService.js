const merchantDb = require('../Database/MerchantDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Merchant = require('../Entities/Merchant');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const merchants = await merchantDb.getMerchant();     
     if(merchants != null){
        console.log(' Merchants retrieved!');
        res.send(merchants);        
     }else{
        console.log(' Merchants not retrieved');
        res.send(' Merchants not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get merchants');
         console.log('Exception: Cannot get merchants');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientMerchant = req.body;                     
        const token = await merchantDb.addMerchant(clientMerchant);
        if (token)
        {
            return res.status(201).send({ auth: true, token: token })
            console.log("merchant added successfully");
        }else{
            return res.status(400).send({ auth: false, token: null })
            console.log("merchant not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: merchant not added");
        return res.send('Exception: merchant not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const JSONId = req.params;                                  
        const id = JSONId.id              
        const o_id = new ObjectId(id);  
        const clientMerchant = req.body; 

        const result = await merchantDb.updateMerchant(clientMerchant,o_id);        
        if(result != null)
        {
            res.send('merchant updated');
            console.log('merchant updated');
        }else{
            console.log('Merchant entry not found');
            res.send('Merchant entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: merchant not updated');
        res.send('Exception: merchant not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const JSONId = req.params;                                
        const id = JSONId.id      
        const o_id = new ObjectId(id);  
        const result = await merchantDb.deleteMerchant(o_id);        
        if(result == 1)
        {
            res.send('Merchant deleted successfully');
            console.log('Merchant deleted successfully');
        }else{
            console.log('Merchant not deleted');
            res.send('Merchant not deleted');
        }
    }catch(error){
        //console.error(error);
        console.log('Controller Exception: Merchant not deleted ');
        res.send('Exception: Merchant not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await merchantDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Merchant found!');
        }else{
            res.send('Merchant not found!');
            console.log('Merchant not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get merchant by id');
        res.send(' Exception: Cannot get merchant');
    }
})

router.get('/username/:user', async (req,res)=>{
    try{        
        const userNameObject = req.params;                
        const username =  userNameObject.user;        
        const result = await merchantDb.getByUsername(username);        
        if(result != null)
        {
            res.send(result);
            console.log('Merchant found!');
        }else{
            res.send('Merchant not found!');
            console.log('Merchant not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get Merchant by username`');
        res.send('Exception: Cannot get Merchant by username');
    }
})



router.post('/login',async (req,res)=>{
    try{
        const clientMerchant  = req.body;
        const username = clientMerchant.username;                     
        const serverMerchant = await merchantDb.getByUsername(username)    
        console.log('The record',serverMerchant);
        
        if (serverMerchant == null)
        {
            console.log('Merchant does not exist');
            res.send('Merchant does not exist');
        }else{
            const  hash = serverMerchant.password;
            console.log(hash);
            
            const  plainTextPassword = clientMerchant.password;            
            const result = await bcrypt.compare(plainTextPassword,hash);            
            const secret = process.env.accessToken;
            const token = jwt.sign({username :clientMerchant.username },secret)
            if(result)
                {
                    console.log('Merchant authenticated');
                    return res.status(201).send({auth: true, token: token});      
                }else{
                    console.log('Incorrect password');
                    return res.status(201).send({auth: false, token: null});  
                }
        }
        }catch(err){
            console.log('Controller Exception: cannot authenticate');
            console.error(err);
            return res.send('Cannot Authenticate');
        }
    })

module.exports = router;