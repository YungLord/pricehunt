import axios from 'axios';

const URL = 'http://10.0.2.2:8080';

class formApiService {
    
    SignUpHunter = async (Hunter) =>{ return await axios.post(URL + '/user/add',Hunter) } 
    SignUpMerchant = async (Merchant) =>{ return await axios.post(URL + '/merchant/add',Merchant) } 
    LoginHunter = async (Hunter) =>{ return await axios.post(URL + '/user/login',Hunter) } 
    LoginMerchant = async (Merchant) =>{ return await axios.post(URL + '/merchant/login',Merchant) }
    
}
export default new formApiService();