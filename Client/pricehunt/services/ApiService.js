import axios from 'axios';

const URL = 'http://10.0.2.2:8080';

class ApiService {
 
    AddItem = async (Item) =>{ return await axios.post(URL + '/item/add',Item) } 
    GetItem = async () =>{ return await axios.get(URL + '/item/all') } 
    GetOneItem = async () =>{ return await axios.get(URL + '/item/5ef8431fa07666138c1602ab') } 
}
export default new ApiService();