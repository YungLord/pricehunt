import {store} from './store/store'
import Router from './router/router'
import React,{useEffect} from 'react';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen'
import { PersistGate } from 'redux-persist/es/integration/react'

class App extends React.Component{

componentDidMount(){
  SplashScreen.hide()
}

render(){
  return (
   <Provider store={store}>
      <Router/>
   </Provider>
  );
 };
};

export default App;
