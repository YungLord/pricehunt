import React from 'react';
//import thunk from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux';
import {allReducers} from '../reducers/globalReducer';

export const store = createStore(allReducers,applyMiddleware());

