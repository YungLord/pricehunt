import React from 'react'
import {Text,StyleSheet,View, Alert,  Modal,TouchableOpacity,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default function RenderItems(props){
    return(
        <View>
        <TouchableOpacity onPress={()=>props.navigation.navigate('Edit') }>
            <View elevation={5} style={styles.rectangle}>
            <View style={{flexDirection:'row'}}>
                <View>                       
                    <Image source={{uri: `data:image/gif;base64,${props.obj.image}`}} style={styles.ImageContainer}/>     
                </View>  
                <View>
                    <View style={{flexDirection:'row'}}>
                      <Text style={styles.ItemNameContainer}>Item Name:</Text>
                      <Text style={styles.ItemNameText}>{props.obj.name}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                      <Text style={styles.PriceConatiner}>Price:</Text>
                      <Text style={styles.PriceText}>${props.obj.price}.00 JMD</Text>
                    </View>
                    <View>
                      <TouchableOpacity style={{position:'absolute'}}> 
                          <Text style={styles.EditItemText}>Edit Item</Text>
                      </TouchableOpacity>
                   </View>
                </View> 
             </View>
            </View>
        </TouchableOpacity>
      <View><Text></Text></View>
    </View>  
  )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      top:30,
      backgroundColor: "white",
    },
   
    rectangle: {
        bottom:20,
      width: 210 * 2,
      height: 120,
      backgroundColor: "white",
      borderRadius:25,
      bottom:10,
      shadowColor: '#000000',
      shadowOffset: {
        width: 3,
        height: 3,
    }
  },
    ItemNameContainer:{
      position:'absolute',top:30,left:15,
      color:'#F85252',fontWeight:'700',fontSize:17,fontStyle:'italic'
    },
    ItemNameText:{
        position:'absolute',top:30,left:110,
        color:'black',fontSize:17,fontStyle:'italic'
      },
    PriceConatiner:{
      position:'absolute',top:55,left:15,
      color:'#F85252',fontWeight:'700',fontSize:17,fontStyle:'italic'
    },
    PriceText:{
        position:'absolute',top:55,left:63,
        color:'black',fontSize:17,fontStyle:'italic'
      },
    EditItemText:{
      textAlign:'right',
      left:220,
      top:85,
      position:'absolute',
      color:'#F85252',fontWeight:'700',fontSize:17,fontStyle:'italic',
      zIndex:20
    },
    ImageContainer:{
      top:5,
      width: 100,
      height: 100 ,
      borderTopLeftRadius:20 
    }
  });