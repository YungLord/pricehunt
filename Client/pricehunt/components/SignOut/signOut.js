import React from "react";
import {connect} from 'react-redux'
import { Alert } from "react-native";
import {store} from '../../store/store'
import {LogoutAction} from '../../actions/logoutAction'
import AsyncStorage from '@react-native-community/async-storage';

export default function SignOut(navigation) {
       Alert.alert("SignOut")
       AsyncStorage.removeItem("User")
       AsyncStorage.removeItem("Token")
       navigation.navigate('SignIn')   
       store.dispatch(LogoutAction(false))
}
