import React from 'react'
import {TouchableOpacity} from 'react-native';
import MenuIcon from '../SvgComponents/Icons/MenuIcon/menuIcon'

export default function MenuButton(props){
    return(
    <TouchableOpacity onPress={()=>props.navigation.navigate('Menu')}>
            <MenuIcon/>
    </TouchableOpacity>
    )
}