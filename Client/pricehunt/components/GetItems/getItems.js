import React, { useEffect, useState } from 'react'
import RenderItems from '../RenderItems/renderItems'
import ApiService from '../../services/ApiService'
import { ReactReduxContext } from 'react-redux'
import {Text,View,ActivityIndicator,StyleSheet,Dimensions,RefreshControl, Alert} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import PTRView from 'react-native-pull-to-refresh';

export default class GetItems extends React.Component{
    
constructor(props){
        super(props)
        this.state = {
            item:[],
            loading:true,
            refreshing: false,
        }
    }

componentDidMount(){
        this.getItem()
    }

getItem = () => {
        ApiService.GetItem().then(res=>res.data).then((res)=>{
            if(res){         
               this.setState({item:res}) 
               this.setState({loading:false}) 
            }    
        })
    }

Fetch = () => {
        return this.state.item.map((data, name) => {   
        return <RenderItems obj={data} key={name}  navigation={this.props.navigation}/>
    });
}

_refreshControl(){
    return (
      <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={()=>this._refreshListView()} />
    )
}      
_refreshListView(){
    this.setState({refreshing:true});
    this.getItem()
    this.setState({refreshing:false});
}

render(){ 
  return(
    
      <ScrollView refreshControl={this._refreshControl()}>         
            <View style={styles.container}>
            { this.state.loading?
                <ActivityIndicator size="large" color="#0000ff"/>
            : this.Fetch()
            }    
            </View>
        <Text>{"\n\n\n\n\n\n"}</Text>
    </ScrollView>
  )
 }
}
const styles = StyleSheet.create({
    container: {  
        alignItems: "center", 
        backgroundColor: 'white',
        padding:25,
      },
})