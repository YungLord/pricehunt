import {connect} from "react-redux"
import React,{useEffect,useState} from 'react'
import {useForm} from 'react-hook-form'
import formApiService from '../../services/formApiService'
import Icon from 'react-native-vector-icons/MaterialIcons';
import LoginIcon from '../SvgComponents/Icons/LoginIcon/loginIcon'
import {LoginAction} from '../../actions/loginAction'
import {HunterAction} from '../../actions/hunterAction'
import{MerchantAction} from '../../actions/merchantAction'
import PasswordIcon from '../SvgComponents/Icons/PasswordIcon/passwordIcon'
import UsernameIcon from '../SvgComponents/Icons/UsernameIcon/usernameIcon'
import AsyncStorage from '@react-native-community/async-storage';
import {StyleSheet,TouchableOpacity,Text,View, Alert, ImagePropTypes,TextInput} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableHighlight } from "react-native-gesture-handler"

function HunterLoginForm(props){
  
  let status = false;
  let hunter = "Hunter"

  const { register, setValue, handleSubmit, errors } = useForm()
  
  const [show, showPassword] = useState(true);
    
  const HunterLogin = (data) => {
             console.log(JSON.stringify(data))        
             formApiService.LoginHunter(data).then((res)=>res.data).then((auth) => {
             if(auth.auth === true){ 
                status = true;
                props.reduxLoginAction(status)
                props.reduxHunterAction(hunter)
                AsyncStorage.setItem("User",data.username)
                AsyncStorage.setItem("Token",auth.token)
                props.navigation.navigate("Hub")        
             } 
             else{
               console.log("Login Failed")
             }
          })
      }

    useEffect(() => {
        register({ name: "username"}, { required: true });
        register({ name: "password"}, { required: true });
    },[register])

        return(

            <View style = {styles.SignUpContainer}>                                 
            <View style={styles.RowContainer}> 
            <View style={styles.UsernameIcon}> 
            <UsernameIcon/>   
            </View>                  
            <TextInput  style={styles.TextInputRegister} placeholder="Username" 
            onChangeText={ text =>  setValue('username', text, true)}/>        
            {errors.username && <Text style={styles.Required}>Required</Text>}               
            </View>
            <View style={styles.RowContainer}>
            <View style={styles.PasswordIcon}> 
                <PasswordIcon/>   
            </View>                               
                <TextInput style={styles.TextInputRegister} placeholder="Password"
                 onChangeText={ text =>  setValue('password', text)} secureTextEntry={show}/>    
                  {show?
               <Icon name="visibility-off" size={25} style={{top:12,right:10}} color="grey" onPress={()=>showPassword(false)}/> 
               :<Icon name="visibility" size={25} style={{top:12,right:10}} color="grey" onPress={()=>showPassword(true)}/>
               }
            {errors.password && <Text style={styles.Required}>Required</Text>}                      
            </View> 
                <TouchableOpacity style={styles.Login} onPress={handleSubmit(HunterLogin)}>
                    <LoginIcon/>
                </TouchableOpacity>  
            </View> 
        );
}
const styles = StyleSheet.create({
    SignUpContainer:{
        position: 'absolute',
        top:hp('10%'),
        marginLeft:10
      },
      PasswordIcon:{
        top:13
      },
      RowContainer:{
        flexDirection:'row'
       },
       UsernameIcon:{
        top:13
      },
      TextInputRegister: {
        height: hp("5%") ,
        width: hp("30%"),
        borderBottomColor:'black',
        borderBottomWidth:1,
        backgroundColor:'#ffffff',
      },
      Login: {       
        height:hp("7%"),
        width:hp("7%"),
        bottom:hp('6.5%'),  
        left:wp('78%'),        
        padding:22,
        borderRadius:60,   
        elevation: 30,
        backgroundColor:'#ffffff',
        borderColor: '#F150D8',   
      },
      Required:{
        right:100,
        top:42,
        zIndex:5,
        fontWeight:"700",
        color:'red',  
      }
})
const mapDispatchToProps = (dispatch) => 
{
    return {
     reduxLoginAction:(status) => dispatch(LoginAction(status)) ,
     reduxHunterAction:(hunter) => dispatch(HunterAction(hunter)) 
  }
}
export default connect(null,mapDispatchToProps)(HunterLoginForm);