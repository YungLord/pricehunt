import React from "react";
import SignOut from '../SignOut/signOut'
import Icon from 'react-native-vector-icons/MaterialIcons';
import MenuSVG1 from '../../components/SvgComponents/MenuSvg/menuSVG1'
import { StyleSheet, Text, View, TouchableOpacity, Animated, Dimensions } from "react-native";

export default function MenuContent (props){
    const navigation = props.navigation
    return(
    <View style={{alignItems:'flex-start'}}>
       
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('Hub')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="home" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>Home</Text>                  
                    </View> 
                   
                </TouchableOpacity>
            </View> 
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('Profile')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="account-circle" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>Profile</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('Favourite')}>
                    <View style={{    flexDirection:'row'}}>
                    <Icon name="menu" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        
                        <Text style={{textAlign:'left'}}>View Favourite List</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('MerchantHub')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="broken-image" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>Merchant Portal</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>

          <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('PriceHub')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="attach-money" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>Price Hub</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('About')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="developer-board" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>About Us</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>
            <View style={styles.container}>  
                <TouchableOpacity onPress={()=>props.navigation.navigate('Settings')}>
                    <View style={{    flexDirection:'row'}}>
                        <Icon name="settings" size={25} style={{right:10,bottom:3}} color="#F85252"/> 
                        <Text style={{textAlign:'left'}}>Settings</Text>                  
                    </View> 
                </TouchableOpacity>
            </View>
            <View style={{alignSelf:'center'}}>
                <TouchableOpacity onPress={()=>SignOut(navigation)}>
                    <Text style={{}}>LogOut</Text>
                </TouchableOpacity>
            </View>
        <View style={styles.MenuSVG1}>
            <MenuSVG1/>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
    container:{
        padding:20,
        justifyContent:'flex-start',
        alignItems:'flex-start',
         
    },
    MenuSVG1:{
       top:10
    }
})