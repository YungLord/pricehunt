import React,{useState} from 'react'
import {Text,StyleSheet,View, Alert,  Modal,TouchableOpacity,Image} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import BusinessIcon from '../SvgComponents/Icons/BusinessIcon/businessIcon'
import ViewHunterItems from '../../components/ViewHunterItems/viewHunterItems'


export default function RenderHunterItems(props){
 
  const [fav,Favourite] = useState(false)

  
    return(
        <View style={styles.container}>
          <TouchableOpacity onPress={()=>ViewHunterItems(props.navigation,props.obj)}>
              <View elevation={5} style={styles.rectangle}>
              <View style={{flexDirection:'row'}}>              
                  <View>
                      <View style={{flexDirection:'row'}}>
                        { fav?
                          <Icon name="heart" size={25} style={{top:12,right:70,position:'absolute',zIndex:5}} color="red" onPress={()=>Favourite(false)}/>:
                          <Icon name="heart-outline" size={25} style={{top:12,right:70,position:'absolute',zIndex:5}} color="grey" onPress={()=>Favourite(true)}/>
                        }
                          <View style={{left:50}}>                  
                          <Image source={{uri: `data:image/gif;base64,${props.obj.image}`}} style={styles.ImageContainer}/>  
                          </View>         
                      </View>
                      <View style={{top:50,left:50}}>
                        <Text style={styles.ItemNameContainer}>
                          {props.obj.name}
                        </Text>
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{top:65,left:20,position:'absolute'}}>
                        <BusinessIcon/>
                        </View>
                        <Text style={styles.BusinessText}>Mega Mart</Text>
                      </View>
                      <View style={{flexDirection:'row'}}>
                      <Icon1 name="attach-money" size={25} style={{top:87,right:60,position:'absolute',zIndex:5}} color="green" onPress={()=>Favourite(false)}/>                       
                          <Text style={styles.PriceText}>{props.obj.price}.00 JMD</Text>
                      </View>          
                  </View> 
              </View>
              </View>
           </TouchableOpacity>
            <View><Text>  </Text></View>   
    </View>
  )
}
const styles = StyleSheet.create({
    container: {
     left:5,
      alignItems: "center",
      backgroundColor: "white",
    },
    rectangle: {
      bottom:20,
      width: 220,
      height: 250,
      backgroundColor: "white",
      borderRadius:25,
      bottom:10,
      left:15,
      top:15,
      shadowColor: '#000000',
      shadowOffset: {
        width: 3,
        height: 3    
    }
  },
    ItemNameContainer:{
    textAlign:'center',
      color:'black',fontWeight:'700',fontSize:17,fontStyle:'italic'
    },
    BusinessText:{
        position:'absolute',top:65,left:50,fontWeight:'700',
        color:'#EFB926',fontSize:17,fontStyle:'italic'
    },
    PriceText:{
        top:90,left:70,
        color:'black',fontSize:17,fontStyle:'italic'
    },
    EditItemText:{
      textAlign:'right',
      left:220,
      top:85,
      position:'absolute',
      color:'#F85252',fontWeight:'700',fontSize:17,fontStyle:'italic',
      zIndex:20
    },
    ImageContainer:{
      top:30,left:50,
      width: 100,
      height: 100 ,left:10,
      borderTopLeftRadius:20 
    }
  });