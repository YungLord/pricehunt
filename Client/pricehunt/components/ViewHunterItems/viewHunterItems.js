import React from 'react'
import {Text} from 'react-native'
import {connect} from 'react-redux'
import {store} from '../../store/store'
import {ViewHunterItemsAction} from '../../actions/viewHunterItemsAction'

export default function ViewHunterItems(navigation, items){

    console.log(items);
    navigation.navigate("View");
    store.dispatch(ViewHunterItemsAction(items))
   // props.reduxViewHunterSelectedItems(items);
}

