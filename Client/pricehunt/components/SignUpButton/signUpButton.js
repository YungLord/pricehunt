import React from "react";
import { Text,TouchableOpacity,View ,StyleSheet, Alert, AsyncStorage} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SignUpIcon from '../LoginButton/LoginIcon/loginIcon'
import formApiServce from '../../services/formApiService'

function SignUpButton(props) {

  const Hunter = {
    hunterFullname:props.fullname, 
    hunterUsername:props.username,
    hunterEmail:props.email,  
    hunterPassword:props.password,
    hunterConfirmedPassword:props.confirmedPassword,
  }

  const SignUp = () => {
       Alert.alert(JSON.stringify(Hunter));
       AsyncStorage.setItem("Hunter",props.username);
       props.navigation.navigate('Main')
    }

  return (
           <TouchableOpacity style={styles.SignUp} onPress={SignUp} >
                  <SignUpIcon/>
            </TouchableOpacity>  
  );
}
const styles = StyleSheet.create({
    SignUp: {
        height:hp("8%"),
        width:hp("8%"),
        top:hp('15%'),  
        right:hp('22%'),        
        padding:15,
        borderRadius:60,   
        elevation: 30,
        backgroundColor:'#ffffff',
        borderColor: '#F150D8',   
      }
})
export default SignUpButton;