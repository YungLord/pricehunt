import React from "react";
import Svg,{Path,G,Rect} from 'react-native-svg'

function SettingsBarIcon() {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width="56"
      height="7"
      viewBox="0 0 56 7"
    >
      <G
        fill="#d4d4d4"
        stroke="#d4d4d4"
        strokeWidth="1"
        data-name="Rectangle 32"
      >
        <Rect width="56" height="7" stroke="none" rx="3"></Rect>
        <Rect width="55" height="6" x="0.5" y="0.5" fill="none" rx="2.5"></Rect>
      </G>
    </Svg>
  );
}

export default SettingsBarIcon;
