import React from "react";
import { Text,TouchableOpacity,View ,StyleSheet, Alert, AsyncStorage} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import formApiServce from '../../services/formApiService'

function SignOutButton(props) {

  return (
    <TouchableOpacity style={{}} onPress={()=>props.navigation.navigate('SignIn')}>
          <Text style={{color:'white',fontWeight:"700",top:35,left:25,zIndex:8}}>Sign Out</Text>
     </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
    SignOut: {
       
      }
})
export default SignOutButton;