import React,{useEffect, useState} from 'react'
import {StyleSheet,TouchableOpacity, Alert} from 'react-native'
import GoogleIcon from '../SvgComponents/Icons/GoogleIcon/googleIcon'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

export default function GoogleLogin(){

  let info = " "

  useEffect(() => {
    GoogleSignin.configure();
  })

    const signInGoogle = async() => {
      try{ 
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn()
        info = userInfo
      }catch(error){
            if(error.code === statusCodes.SIGN_IN_CANCELLED){
              console.log("User cancelled sign in");
            } else if (error.code === statusCodes.IN_PROGRESS){
               console.log("Loading");
            } else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
              console.log("PlayServices is unvailable");
            } else {
              console.log(error);
          }
       }
       console.log(JSON.stringify(info))
    }

    return(
         <TouchableOpacity style={styles.ButtonRegister} onPress={signInGoogle}>
           <GoogleIcon/>
         </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    ButtonRegister: {
    height:50,
    width:50,
    padding:11,
    marginTop:20,
    marginLeft:8,
    backgroundColor:'#ffffff',
    borderRadius:60,
    borderColor: '#F150D8',
    elevation: 50
  }
})