import React from 'react';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ApiService from '../../services/ApiService'
import {Text,StyleSheet,View,Modal,TextInput,TouchableOpacity} from 'react-native'


export default class AddItems extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            encodedBase64:'',
            photo:'',
            image:'',
            modalVisible:false,
            name:'',type:'',price:'',ID:'', photo:'',image:'',
            item:[],test:'',test1:'',
             url:'',loading:true
        }
    }

addItem = () => {
    let Item = {name:this.state.name,type:this.state.type,price:this.state.price,merchantId:this.state.ID,image:this.state.encodedBase64}
    console.log(JSON.stringify(Item))
    ApiService.AddItem(Item).then(res=>res.data).then((res)=>{
        if(res){
            console.log(res)    
        }
    })
}

chooseImage = () => {
  const options = {};
    ImagePicker.launchImageLibrary(options,(response)=>{
        if(response.uri){
            console.log(response)
            this.setState({photo : response})
            this.setState({encodedBase64 : response.data})
        }
    })
}

onChangeText = (key, val) => {
    this.setState({ [key]: val})
}

setModalVisible = () => {
    this.setState({modalVisible:!this.state.modalVisible})
}

render(){
  return(
   <>
    <View style={styles.container}>  
    <Text>{"\n\n\n"}</Text>
    <TouchableOpacity onPress={this.setModalVisible}>   
        <View style={{flexDirection:'row',position:'relative'}}>    
            <Text style={{color:'grey',bottom:5,right:7}}>________________________________</Text>
            <Icon name="add" size={25} style={{bottom:3,right:5}} color="black"/>
            <Text style={{color:'#EFB926',fontWeight:'bold'}}>Add New Item</Text>
        </View>
    </TouchableOpacity>

    <View style={styles.centeredView}>
        <Modal animationType="slide" transparent={true} visible={this.state.modalVisible}>
            <View style={styles.centeredView}>
            <View style={styles.modalView}>
                <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
                    <TextInput style = {styles.TextInput} placeholder="Name"  name="name" onChangeText={val => this.onChangeText('name', val)}></TextInput>
                    <Text>{"\n\n"}</Text>
                </View>
                <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
            
                <TextInput style = {styles.TextInput} placeholder="Type"  name="type" onChangeText={val => this.onChangeText('type', val)}></TextInput>
                <Text>{"\n\n"}</Text>
                </View>
                <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
        
                <TextInput style = {styles.TextInput} placeholder="Price" name="price" onChangeText={val => this.onChangeText('price', val)}></TextInput>
                <Text>{"\n\n"}</Text>
                </View>
                <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
            
                <TextInput style = {styles.TextInput} placeholder="ID" name="ID" onChangeText={val => this.onChangeText('ID', val)}></TextInput>
                <Text>{"\n\n\n"}</Text>
                </View>
                <Text>Image</Text>
                <TouchableOpacity onPress={this.chooseImage}>
                    <Text>Upload Photo</Text>                             
                </TouchableOpacity>
                <Text>{"\n"}</Text>
                <TouchableOpacity onPress={this.addItem}>
                    <Text>Add</Text>                             
                </TouchableOpacity>

                <TouchableOpacity onPress={ this.setModalVisible}>
                <Text>Hide Modal</Text>
                </TouchableOpacity>
            </View>
         </View>
       </Modal>
    </View>
</View>
</>
    )
  }
}
const styles = StyleSheet.create({

container:{
    flex:1,
    backgroundColor:'white',
    alignItems:'center'
   },
   centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    height:400,width:350,

    alignItems: "center",
    shadowColor: "#ffffff",
    shadowOffset: {
      width: 20,
      height: 20
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  TextInput:{
    backgroundColor:'white',
    width:220,
    height:40,
    bottom:15,
    top:15,
    borderBottomColor:'grey',
    borderBottomWidth:0.5,
},
})