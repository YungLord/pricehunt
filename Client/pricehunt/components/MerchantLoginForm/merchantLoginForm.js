import {connect} from 'react-redux'
import React,{useEffect,useState} from 'react'
import {useForm} from 'react-hook-form'
import Icon from 'react-native-vector-icons/MaterialIcons';
import formApiService from '../../services/formApiService'
import {LoginAction} from '../../actions/loginAction'
import {MerchantAction} from '../../actions/merchantAction'
import AsyncStorage from '@react-native-community/async-storage';
import EmailIcon from '../SvgComponents/Icons/EmailIcon/emailIcon'
import LoginIcon from '../SvgComponents/Icons/LoginIcon/loginIcon'
import PasswordIcon from '../SvgComponents/Icons/PasswordIcon/passwordIcon'
import {StyleSheet,TouchableOpacity,Text,View,TextInput,Alert} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

function MerchantLoginForm(props){
  
  let status = false
  let merchant = "Merchant";

  const { register, setValue, handleSubmit, errors } = useForm();

  const [show, showPassword] = useState(true);

  const MerchantLogin = (data) => {
      console.log(JSON.stringify(data))        
      formApiService.LoginMerchant(data).then((res)=>res.data).then((auth) => {
      if(auth.auth === true){ 
        status = true;
        props.reduxLoginAction(status)
        props.reduxMerchantAction(merchant)
        AsyncStorage.setItem("User",data.username)
        AsyncStorage.setItem("Token",auth.token)
        props.navigation.navigate("Hub")        
      } 
      else{
        console.log("Login Failed")
      }
  })
}
 
useEffect(() => {
    register({ name: "username"},{ required: true });
    register({ name: "password"},{ required: true });
}, [register]);

return(
        <View style = {styles.SignUpContainer}>                                 
          <View style={styles.RowContainer}> 
              
            <View style={styles.EmailIcon}> 
              <EmailIcon/>   
            </View>                  
             <TextInput style={styles.TextInputRegister} placeholder="Business Email Address" 
              onChangeText={text => setValue("username", text, true)}/>             
               {errors.Email?.type === "required" && <Text style={styles.Errors}>Required.</Text>}
               {errors.Email?.type === "maxLength" && <Text style={styles.Errors}>Too Long.</Text>}             
          </View>
          <View style={styles.RowContainer}>
          <View style={styles.PasswordIcon}> 
              <PasswordIcon/>   
          </View>                               
              <TextInput style={styles.TextInputRegister} placeholder="Password" 
               onChangeText={text => setValue("password", text, true)} secureTextEntry={show}/>  
               {show?
               <Icon name="visibility-off" size={25} style={{top:12,right:10}} color="grey" onPress={()=>showPassword(false)}/> 
               :<Icon name="visibility" size={25} style={{top:12,right:10}} color="grey" onPress={()=>showPassword(true)}/>
               }
              {errors.Password?.type === "required" && <Text style={styles.Errors}>Required.</Text>}
              {errors.Password?.type === "maxLength" && <Text style={styles.Errors}>Too Long.</Text>}
          </View>        
          <View> 
              <TouchableOpacity style={styles.Login} onPress={handleSubmit(MerchantLogin)}>
                  <LoginIcon/>
              </TouchableOpacity>  
          </View> 
        </View> 
        
        );
}
const styles = StyleSheet.create({
    SignUpContainer:{
        position: 'absolute',
        top:hp('10%'),
        marginLeft:10
      },
      PasswordIcon:{
        top:13
      },
      RowContainer:{
        flexDirection:'row'
       },
      EmailIcon:{
        top:15
      },
      TextInputRegister: {
        height: hp("5%") ,
        width: hp("30%"),
        borderBottomColor:'black',
        borderBottomWidth:1,
        backgroundColor:'#ffffff',
      },
      Login: {       
        height:hp("7%"),
        width:hp("7%"),
        bottom:hp('6.5%'),  
        left:wp('78%'),        
        padding:22,
        borderRadius:60,   
        elevation: 30,
        backgroundColor:'#ffffff',
        borderColor: '#F150D8',   
      },
      Errors:{
       right:100,
       top:42,
       zIndex:5   
      }
})
const mapDispatchToProps = (dispatch) => 
{
    return {
     reduxLoginAction:(status) => dispatch(LoginAction(status)) ,
     reduxMerchantAction:(merchant) => dispatch(MerchantAction(merchant)) 
  }
}
export default connect(null,mapDispatchToProps)(MerchantLoginForm);