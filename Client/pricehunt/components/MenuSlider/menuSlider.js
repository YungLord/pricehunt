import React,{useEffect} from "react";
import { StyleSheet, Text, View, TouchableOpacity, Animated, Dimensions } from "react-native";
import MenuContent from '../MenuContents/menuContent'
import SettingsBarIcon from '../../components/SvgComponents/Icons/SettingsBarIcon/settingsBarIcon'

export default function MenuSlider(props) {
 
  const animation= new Animated.Value(0)
  const screenHeight = Dimensions.get("window").height;

  const backdrop = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0, 0.01],
          outputRange: [screenHeight, 0],
          extrapolate: "clamp",
        }),
      },
    ],
    opacity: animation.interpolate({
      inputRange: [0.01, 0.5],
      outputRange: [0, 1],
      extrapolate: "clamp",
    }),
  };

  const slideUp = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0.01, 1],
          outputRange: [0, -1 * screenHeight],
          extrapolate: "clamp",
        }),
      },
    ],
  };

useEffect(() => {
    handleOpen()
  })

  const handleOpen = () => {
    Animated.timing(animation, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };
  const handleClose = () => {
    Animated.timing(animation, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

    return (
        <Animated.View style={[StyleSheet.absoluteFill, styles.cover, backdrop]}>
          <View style={[styles.sheet]}>
            <Animated.View  style={[styles.popup, slideUp]}>
              <TouchableOpacity onPress={handleClose}>
                <Text>{"\n"}</Text>
              <SettingsBarIcon/>
              </TouchableOpacity>
                  <MenuContent navigation={props.navigation}/>
            </Animated.View>
          </View>
        </Animated.View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",

  },
  cover: {
    backgroundColor: "rgba(0,0,0,.5)",

    elevation:40,
  },
  sheet: {
    position: 'absolute',
    top: Dimensions.get("window").height,
    left: 0,
    right: 0,
    height: "100%",
    justifyContent: "flex-end",

  },
  popup: {
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    minHeight: 80,
  },
});
