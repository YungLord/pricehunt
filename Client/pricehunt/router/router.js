import 'react-native-gesture-handler';
import React from 'react';
import HubScreen from '../screens/HubScreen/hubScreen';
import AboutScreen from '../screens/AboutScreen/aboutScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from '../screens/SignInScreen/signInScreen';
import SignUpScreen from '../screens/SignUpScreen/signUpScreen';
import ProfileScreen from '../screens/ProfileScreen/profileScreen';
import PriceHubScreen from '../screens/PriceHubScreen/priceHubScreen' 
import SettingsScreen from '../screens/SettingsScreen/settingsScreen';
import MerchantHubScreen from '../screens/MerchantHub/merchantHubScreen';
import FavouriteScreen from '../screens/FavouriteScreen/favouriteScreen'
import ViewHunterItemsScreen from '../screens/ViewHunterItemsScreen/viewHunterItemsScreen'

    
const Stack = createStackNavigator();

export default function Router(){

  return(
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false}}>
      <Stack.Screen name="Hub" component={HubScreen}/>
      <Stack.Screen name="SignIn" component={SignInScreen}/>
      <Stack.Screen name="SignUp" component={SignUpScreen}/>     
      <Stack.Screen name="Profile" component={ProfileScreen}/>
      <Stack.Screen name="PriceHub" component={PriceHubScreen}/>
      <Stack.Screen name="MerchantHub" component={MerchantHubScreen}/>
      <Stack.Screen name="Favourite" component={FavouriteScreen}/>
      <Stack.Screen name="Settings" component={SettingsScreen}/>
      <Stack.Screen name="About" component={AboutScreen}/>
      <Stack.Screen name="View" component={ViewHunterItemsScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
