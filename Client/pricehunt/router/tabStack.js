import 'react-native-gesture-handler';

import * as React from 'react';
import {Text, Alert} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import GetItems from '../components/GetItems/getItems';
import EditItems from '../components/EditItems/editItems'
import AddItems from '../components/AddItems/addItems';
import ManagePromotions from '../components/ManagePromotions/managePromotions';
import ViewStatistics from '../components/ViewStatistics/viewStatistics'

const Tab = createMaterialTopTabNavigator();

export default function TabStack() {
    return (
      <>
        <Tab.Navigator 
        initialRouteName="View"
        tabBarOptions={{
          style: {
            backgroundColor: 'white',
          },
        }}>
        <Tab.Screen
          name="View"
          component={GetItems}
          options={{
            tabBarLabel: 'View',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="home" color={color} size={size} />
            // ),
          }}  />
        <Tab.Screen
          name="Add"
          component={AddItems}
          options={{
            tabBarLabel: 'Add Items',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
          <Tab.Screen
          name="Statistics"
          component={ViewStatistics}
          options={{
            tabBarLabel: 'Stats',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
          <Tab.Screen
          name="Edit"
          component={EditItems}
          options={{
            tabBarLabel: 'Edit Items',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
          <Tab.Screen
          name="Promotions"
          component={ManagePromotions}
          options={{
            tabBarLabel: 'Promo',
            // tabBarIcon: ({ color, size }) => (
            //   <MaterialCommunityIcons name="settings" color={color} size={size} />
            // ),
          }} />
      </Tab.Navigator>
    </>
    );
  }