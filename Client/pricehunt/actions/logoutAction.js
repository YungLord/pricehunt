export const LogoutAction = () => {
    return{
        type:"LOGOUT",
        payload:{
            login:false
        }
    }
}