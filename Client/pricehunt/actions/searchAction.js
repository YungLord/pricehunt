//import {Alert} from 'react-native'
export const SearchAction = (search) =>{
   return{
       type:"SEARCH",
       payload:{
           searchParam:search
       }
   }
}
