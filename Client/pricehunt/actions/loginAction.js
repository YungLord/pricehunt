export const LoginAction = (status) => {

  return {
    type:"LOGIN",
    payload: {
        status:status
    }
  }
};