import React from 'react'
import {connect} from 'react-redux'
import {View, Text, StyleSheet, Dimensions, Alert} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';

class ViewHunterItemsScreen extends React.Component{
    constructor(props){
        super(props)

        this.state = {
        }
    }

    render(){
        return(
            <View style={styles.body}> 
             <Icon1 name="arrow-left" size={25} color="black" style={{top:15,left:5}} onPress={()=>this.props.navigation.navigate('PriceHub')}/>
            <View style={styles.container}>
                <Text style={{fontWeight:'700',fontSize:18}}>Item Selected{"\n"}</Text> 
                  <Text style={styles.Text}>Name:  {this.props.item.name}</Text>
                  <Text style={styles.Text}>Price:  {this.props.item.price}</Text>
                  <Text style={styles.Text}>Type:  {this.props.item.type}</Text>
                  <Text style={styles.Text}>MerchantID:  {this.props.item.merchantId}</Text> 
            </View>
            
            <View style={styles.Subscribe}>
                <View>
                    <Text style={{fontWeight:'bold',bottom:10}}>Merchant: MegaMart</Text>
                </View>
                <TouchableOpacity style={{backgroundColor:'#0F9BC1', borderRadius:30,width: 75,height:30,padding:6.5}} onPress={()=>Alert.alert("Subscribed to MegaMart")}>
                        <Text style={{color:'white',fontSize:12,left:2}}>Subscribe</Text>
                </TouchableOpacity>
            </View>
        </View>
        )
    }
}
const styles = StyleSheet.create({
  body:{
      backgroundColor:'white',
      height:Dimensions.get('window').height
  },
    container:{
      alignItems:'center',
      top:250
  } ,
  Text:{
      fontSize:20
  },
  Subscribe:{
      top:400,
      left:15,
  } 
})

const mapStateToProps = (state) =>{
    return {
        item: state.item.SelectedItem
    }
}
export default connect(mapStateToProps)(ViewHunterItemsScreen)