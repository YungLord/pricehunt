import React from 'react';
import {Text,StyleSheet,View, Alert,Dimensions,ActivityIndicator} from 'react-native';
import { TouchableOpacity, TextInput, ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import MenuSlider from '../../components/MenuSlider/menuSlider'
import Icon from 'react-native-vector-icons/MaterialIcons';
import MenuIcon from '../../components/SvgComponents/Icons/MenuIcon/menuIcon'
import ProfileIcon from '../../components/SvgComponents/Icons/ProfileIcon/profileIcon'
import SortIcon from '../../components/SvgComponents/Icons/SortIcon/sortIcon'
import HubSearch from '../../components/HubSearch/hubSearch'
import RenderFavouriteItems from '../../components/RenderFavouriteItems/renderFavouriteItems'
import ApiService from '../../services/ApiService'

class FavouriteScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
           test:"",
           user:'',
           item:[],loading:true,val:true
        }
    }


    componentDidMount(){
        this.getUser()
        this.getItem()
    }

    Change = () => {
        this.setState({showMenu:true})
     }

    getUser = async() =>{
    var user = await AsyncStorage.getItem("User")
    this.setState({user:user})        
    }

    Items() {
        return this.state.item.filter((data) => data.name == "Favourites").map((data,name) =>{   
            if(this.state.val){
                return <Text>No Favourite Items</Text>
            } else{
           return <RenderFavouriteItems obj={data} key={name} />}
       });
    }

    getItem = () => {
        ApiService.GetItem().then(res=>res.data).then((res)=>{
            if(res){         
               this.setState({item:res}) 
               this.setState({loading:false}) 
            }    
        })
    }

    render(){
        return(
            <>
            <View style={styles.body}>
                <View style={{flexDirection:"row",left:10,top:5}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}>
                        <ProfileIcon/>
                    </TouchableOpacity>
                    <View>
                        <Text style={{color:'white',fontWeight:"700",left:5,top:7}}>{this.state.user}</Text>
                    </View>
                </View>
                <View style={{alignSelf:'flex-end',justifyContent:'flex-end',position:'absolute',top:5,right:5}}>
                <Icon name="menu" size={35} style={{bottom:8,right:2}} color="white" onPress={this.Change}/>
                 </View>
                <View style={{alignSelf:'center',top:10}}>
                    <Text style={{color:'white',fontWeight:"700",fontSize:20 }}>Favourite Items</Text>
                </View>
                <View>
                    <HubSearch/>
                </View>
                <View style={{flexDirection:'row',top:24,left:17}}>
                
                    <Text style={{color:'white',fontWeight:'700'}}>  Sort </Text>
    
                    <Icon name="arrow-drop-down" size={35} style={{bottom:8,right:10}} color="white"/>
                    <View style={{top:6,right:7}}>
                      <SortIcon/>
                    </View>
                </View>
            </View>
            <View style={styles.container}>
                <ScrollView>
                    <Text>{"\n\n"}</Text>             
                    { this.state.loading?
                     <ActivityIndicator size="large" color="#0000ff"/>
                    : this.Items()
                    }          
                </ScrollView>                   
            </View>
            {
                this.state.showMenu?
                <MenuSlider navigation={this.props.navigation}/>
                : null         
            }    
            </>
        )
    }
}
export default FavouriteScreen;

const styles = StyleSheet.create({
    body: {
        backgroundColor: '#F85252',
        flex:1
      }, 
      container: {
        flex:1,
        alignItems: "center",
        borderTopLeftRadius:50,
        borderTopRightRadius:50,
        backgroundColor: 'white',
        position:'absolute',
        height:Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        top:135
      },
    })