import React,{Suspense,lazy} from 'react';
import {Text,StyleSheet,View, Alert,Dimensions,ActivityIndicator} from 'react-native';
import { TouchableOpacity, TextInput, ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MenuSlider from '../../components/MenuSlider/menuSlider'
import MenuIcon from '../../components/SvgComponents/Icons/MenuIcon/menuIcon'
import ProfileIcon from '../../components/SvgComponents/Icons/ProfileIcon/profileIcon'
import SortIcon from '../../components/SvgComponents/Icons/SortIcon/sortIcon'
import HubSearch from '../../components/HubSearch/hubSearch'
import RenderHunterItems from '../../components/RenderHunterItems/renderHunterItems'
import ApiService from '../../services/ApiService'
import LazyLoad from 'react-lazyload'
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import GetItem from '../../components/GetItems/getItems'
import {connect} from 'react-redux'

class PriceHubScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
           test:"",
           user:'',
           item:[],
           width: 110,
           loading:true,
           search:"Iphone6S"
        }
    }

componentDidMount(){
        this.getUser()
        this.getItem()
    }

getItem = () => {
        ApiService.GetItem().then(res=>res.data).then((res)=>{
            if(res){         
               this.setState({item:res}) 
               this.setState({loading:false})
            }    
        })
}

Change = () => {
        this.setState({showMenu:true})
}

getUser = async() =>{  
    var user = await AsyncStorage.getItem("User")
    this.setState({user:user})                
}

    Items(search) {
        return this.state.item.filter((data) => data.name == search).map((data,name) =>{   
           return <RenderHunterItems obj={data} key={name} navigation={this.props.navigation}/>
        });
    }
    onLayout(e) {
        if (this.state.width !== e.nativeEvent.layout.width) {
          this.setState({
            width: e.nativeEvent.layout.width
          })
        }
      }

    render(){
        const width = `${200 / parseInt(this.state.width / 110)}%`;
        const Items = React.lazy(() => import('../../components/GetItems/getItems')); // Lazy-loaded
        return(
            <>
            <View style={styles.body}>
                <View style={{flexDirection:"row",left:10,top:5}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}>
                        <ProfileIcon/>
                    </TouchableOpacity>
                    <View>
                        <Text style={{color:'white',fontWeight:"700",left:5,top:7}}>{this.state.user}</Text>
                    </View>
                </View>
                <Icon1 name="arrow-left" size={25} color="white" style={{top:15,left:5}} onPress={()=>this.props.navigation.navigate('Hub')}/>
                <View style={{alignSelf:'flex-end',justifyContent:'flex-end',position:'absolute',top:5,right:5}}>
                <Icon name="menu" size={35} style={{bottom:8,right:2}} color="white" onPress={this.Change}/>
                 </View>
                <View style={{alignSelf:'center',top:10}}>
                    <Text style={{color:'white',fontWeight:"700",fontSize:20 }}>Price Hub</Text>
                </View>
                <View>
                    <HubSearch/>
                </View>
                <View style={{flexDirection:'row',top:24,left:17}}>
                   <Text style={{color:'white',fontWeight:'700'}}>  Sort </Text>
                    <Icon name="arrow-drop-down" size={35} style={{bottom:8,right:10}} color="white"/>
                       <View style={{top:6,right:7}}>
                           <SortIcon/>
                      </View>
                </View>
            </View>
            <View style={styles.container} onLayout={this.onLayout.bind(this)}>
                <ScrollView>    
                <View style={styles.container2} onLayout={this.onLayout.bind(this)}>               
                    { this.state.loading?
                     <ActivityIndicator size="large" color="#0000ff" style={{etop:100}}/>
                    : this.Items(this.props.search)
                    }                
                 </View>  
                 <Text>{"\n\n\n\n\n\n\n\n\n"}</Text>       
               </ScrollView>       
            </View>
            {
            this.state.showMenu?
            <MenuSlider navigation={this.props.navigation}/>
            : null         
            }  
            </>
        )
    }
}
const styles = StyleSheet.create({
    body: {
        backgroundColor: '#F85252',
        flex:2
      }, 
      container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop: 40,
        borderTopLeftRadius:40,
        borderTopRightRadius:40,
        backgroundColor: 'white',
        position:'absolute',
        height:Dimensions.get('window').height,
        width:Dimensions.get('window').width,
        top:170
      },  
      container2: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
        flexDirection: 'row',
        flexWrap: 'wrap',
        right:20
      },
      wrapper: {
        marginVertical: 10, alignItems: 'center'
      }
    })
    
const mapStatetoProps = (state)=>{
     return {
            search : state.search.searchParam,
        }
}
export default connect(mapStatetoProps)(PriceHubScreen);