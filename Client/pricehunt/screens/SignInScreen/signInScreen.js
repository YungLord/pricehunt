import React,{useEffect} from 'react';
import {useForm} from "react-hook-form";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {
TouchableOpacityComponent, Alert,
ScrollView,TouchableOpacity, TextInput,
SafeAreaView,StyleSheet,View,Text,StatusBar,
TextInputComponent, Button, TouchableWithoutFeedback
} from 'react-native';

                    //EXTERNAL COMPONENTS//                    
 import {connect} from "react-redux"
import {LoginAction} from '../../actions/loginAction'
import MainSVG from '../../components/SvgComponents/MainSvg/Main/mainSVG';
import SignInSVG from '../../components/SvgComponents/MainSvg/SignIn/signinSVG';
import GoogleLogin from '../../components/GoogleLogin/googleLogin';
import FacebookLogin from '../../components/FacebookLogin/facebookLogin';
import TwitterLogin from '../../components/TwitterLogin/twitterLogin';
import WhiteSVG from '../../components/SvgComponents/MainSvg/WhiteSVG/whiteSVG'
import PinkSVG from '../../components/SvgComponents/MainSvg/PinkSVG/pinkSVG'
import HunterLoginForm from '../../components/HunterLoginForm/hunterLoginForm'
import MerchantLoginForm from '../../components/MerchantLoginForm/merchantLoginForm'
import HomeIcon from '../../components/SvgComponents/Icons/HomeIcon/homeIcon'
import formApiService from '../../services/formApiService'


const color  = "white"

class SignInScreen extends React.Component {
    constructor(props){
      super(props)
      this.state = {
            Consumer: 'Hunter',
            color:"white",
            color1:'black',
            status:true,
      }
  }

Hunter = () => {
  this.setState({Consumer:'Hunter'})
  this.setState({color:'white'})
  this.setState({color1:'black'})
}

Merchant = () => {
  this.setState({Consumer:'Merchant'})
  this.setState({color:'black'})
  this.setState({color1:'white'})
  formApiService
}

render() {
  return (
    <> 
     <ScrollView>
       <View style={styles.body}>    
          <View style ={styles.JoinUsContainer}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('SignUp')}>
                <Text style={styles.JoinUsText}>Join Us</Text>
                </TouchableOpacity>
           </View>
            <View style={styles.SVG1}>               
                  <MainSVG/>                 
            </View>
            <View>            
                <Text style={styles.sectionHeading}>Sign In</Text>
            </View>
              <View style={styles.sectionContainer}>      
                <View style={styles.RowContainer}>  
                    <View style={styles.SVG2}>
                          <SignInSVG/>
                      </View>
                      <View style={styles.RowContainer}>
                          <View style={styles.HunterSVGContainer}>
                            <TouchableOpacity onPress={this.Hunter}>
                              <View style={styles.HunterText}>
                                <Text  style={{fontSize:11, color:this.state.color1}}>Hunter</Text>
                              </View>
                                {
                                this.state.Consumer == 'Hunter'?
                                 <WhiteSVG/>
                                :<PinkSVG/>
                                }
                            </TouchableOpacity>
                          </View>                                                                                  
                          <View style={styles.MerchantSVGContainer}>
                              <TouchableOpacity onPress={this.Merchant}>
                                <View style={styles.MerchantText}>
                                  <Text style={{fontSize:10, color:this.state.color}}>Merchant</Text>
                                </View>
                                  
                                  {
                                  this.state.Consumer == 'Merchant'?
                                  <WhiteSVG/> 
                                  :<PinkSVG/>
                                  }
                              </TouchableOpacity>
                          </View>
                      </View>
                </View> 
                  {    
                    this.state.Consumer == 'Hunter'?
                    <HunterLoginForm navigation={this.props.navigation}/>
                    :<MerchantLoginForm navigation={this.props.navigation}/>
                  }   
                  <View style={styles.SocialMedia}>                   
                      <GoogleLogin/>
                      <FacebookLogin/>
                      <TwitterLogin/>           
                  </View>                                               
                  <View>   
                      <Text style={styles.sectionFooter}>{'\n\n'}Let Us Help You Save</Text>
                      <Text>{'\n'}</Text>
                  </View> 
                  <Text>{'\n\n\n\n\n\n'}</Text>
                  <View style={styles.HomeIcon}>
                      <TouchableOpacity onPress={()=>this.props.navigation.navigate('Hub')}>                 
                        <HomeIcon/>                       
                      </TouchableOpacity>
                  </View>
             </View>
             <Text>{'\n\n\n'}</Text>
         
        </View>
    </ScrollView>
  </>
  );
 };
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: '#F85252',
    flex:1
  },
  SVG1:{
    backgroundColor: '#F85252',
    alignItems: 'flex-end',
  },
  SVG2:{
    right:8,
    bottom:30
  },
  JoinUsContainer:{
    alignSelf:"flex-end",
    position:"absolute",
    zIndex:5,
    right:hp('2.5%'),
    top:hp('4%')
  },
  JoinUsText:{
    fontSize:hp('1.5%'),
    fontFamily:"Segeo UI",
    color:"white"
  },
  RowContainer:{
   flexDirection:'row'
  },
  HunterSVGContainer:{
    right:320,
    top:8
  },
  HunterText:{
    position:'absolute',
    zIndex:5,
    top:15,
    left:10,
  },
  MerchantSVGContainer:{
   right:340,
  top:10
  },
  MerchantText:{
    position:'absolute', 
    zIndex:5,
    top:20,
    left:7,
 
  },
 
  UsernameIcon:{
    top:13
  },
  PasswordIcon:{
    top:13
  },
  SocialMedia:{
    flexDirection:"row",
  },
  HomeIcon:{
    alignSelf:"flex-end",
    right:15,

  },
  scrollView: {
    backgroundColor: '#F85252',
  },
  sectionContainer: {
    backgroundColor: '#F85252',
  },
  sectionHeading: {
    fontSize: 26,
    fontWeight: "600",
    color:'white',
    backgroundColor: '#F85252',
    textAlign:"center",
  },
  sectionFooter:{
    fontSize: 30,
    fontWeight: "600",
    color:'white',
    backgroundColor: '#F85252',
    textAlign:"center",top:40
  },
  ButtonText:{
    fontSize: 18,
    color: '#F45C52',
    textAlign:"center",
    fontWeight: '700',
  },
  Scaler:{
    height:hp('55%'),
    width:hp("55%"),
    zIndex:5
  }
});

const mapDispatchToProps = (dispatch) => 
{
    return{
     reduxLoginAction:(status) => dispatch(LoginAction(status)) 
    }
}
export default connect(null,mapDispatchToProps)(SignInScreen);