import React from'react'
import {connect} from 'react-redux'
import { SafeAreaView } from 'react-native-safe-area-context'
import {Text,View,StyleSheet,Image} from 'react-native'
import {useForm} from 'react-hook-form'
import SignOut from '../../components/SignOut/signOut'
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';

import AsyncStorage from '@react-native-community/async-storage';
import ProfileSvg1 from '../../components/SvgComponents/ProfileSvg/profileSVG1'
import LineIcon from '../../components/SvgComponents/Icons/LineIcon/lineIcon'
import HomeIcon from '../../components/SvgComponents/Icons/HomeIcon/homeIcon'
import MenuIcon from '../../components/SvgComponents/Icons/MenuIcon/menuIcon'
import MenuSlider from '../../components/MenuSlider/menuSlider'
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler'
import FavouriteIcon from '../../components/SvgComponents/Icons/FavoriteIcon/favouriteIcon'

class ProfileScreen extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            favorite: 0,
            account:' ', 
            name:'',
            photo:'',
            encodedBase64:'',
            image:'',file:''
        }
    }

    async componentDidMount() { 
        this.accountType()
        var image = await AsyncStorage.getItem("Image")
        var json = JSON.parse(image)
        this.setState({image:json})
        console.log(json)
    }

    chooseImage = () => {
        const options = {};
         ImagePicker.launchImageLibrary(options,(response)=>{
             console.log(response.data);
             if(response.uri)
             {
                 this.setState({photo : response})
                 this.setState({encodedBase64 : response.data})
             }
         })
    }

    accountType = async() => {
        if(this.props.account == "Hunter"){ 

            var user = await AsyncStorage.getItem("User")
            this.setState({name:user})
            this.setState({account:"Hunter"})           
        }
        else {
            var user = await AsyncStorage.getItem("User")
            this.setState({name:user})
            this.setState({account:"Merchant"})
        }
    }

    Change = () => {
        this.setState({showMenu:true})
     }

    render(){
         this.state.file = {uri: `data:image/gif;base64,${this.state.encodedBase64}`,isStatic: true}
         AsyncStorage.setItem("Image",JSON.stringify(this.state.file ))
        
        return(
        <>
          <ScrollView style={styles.body}>
           <View style={styles.body}> 
               <View style={{flexDirection:'row'}}>
                   <View style={{alignSelf:'flex-start'}}>
                        <ProfileSvg1 />     
                   </View>
                    <View style={{position:'absolute',alignSelf:'flex-start',zIndex:5}}>
                        
                    </View>
                    <View style={{alignItems:'flex-end',flex:1,right:10,top:10}}>
                    <Icon name="menu" size={35} style={{top:5,right:5}} color="white" onPress={this.Change}/> 
               <Text style={{fontSize:30,color:'white',fontWeight:"600",right:35,top:35}}>{this.state.account} Profile</Text>
                    </View>
               </View>

               <View style={{alignSelf:'center'}}>
                   <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={styles.ProfilePhoto} onPress={this.chooseImage}>
                        { this.state.photo &&(
                        
                        <Image source={this.state.image} style={{ width: 250, height: 250, borderRadius: 200  }}/>
                        )}         
                    </TouchableOpacity>
                    <View style={{position:'absolute',alignSelf:'center',left:80}}>
                  
                    </View>
                   </View>
                   <Text style={{textAlign:'center',fontSize:25,color:'white',fontWeight:"700"}}>{this.state.name}</Text>
                   <View style={{flexDirection:'row',alignContent:'center',justifyContent:'center'}}>
                       <Text style={{color:'white'}}>Favourite Items</Text>
                       <LineIcon/>
                       <Text style={{color:'white'}}>Ratings</Text>
                   </View>
                   <View style={{flexDirection:'row',textAlign:'center',alignItems:'center',justifyContent:'space-evenly'}}>
                      <View style={{flexDirection:'row'}}>
                       <Text style={{color:'white'}}>{this.state.favorite} </Text>
                       <FavouriteIcon/>
                       
                       <Text style={{color:'white'}}> ....</Text>
                      </View>
                       
                   </View>
                   <View style={{alignItems:'center',top:20}}>
                       <TouchableOpacity style={styles.FavouriteItemsButton} onPress={()=>this.props.navigation.navigate("Favourite")}>
                           <Text style={{color:'white',textAlign:'center'}}>
                               View Favourite Items
                           </Text>
                       </TouchableOpacity>
                   </View>
                </View>

                <View style={{alignSelf:'flex-end',justifyContent:'flex-end'}}>
                   <TouchableOpacity style={styles.DonateButton}>
                           <Text style={{color:'white',textAlign:'center'}}>
                               Donate
                           </Text>
                       </TouchableOpacity>
                </View>
    
                <View style={{alignItems:'center'}}>
                    <Text>{'\n'}</Text>
                    <Text style={{color:'white',fontWeight:"700",fontSize:20}}>Delete Account</Text>
                    <TextInput style={{backgroundColor:'white',height:90,width:340,borderRadius:10,top:10,elevation:30}}></TextInput>       
                </View>     
                <View style={{top:10}}>
                    <Text style={{textAlign:'right',color:'white',right:60,fontWeight:"700"}}>Optional</Text>
                </View>
                <View style={{left:50}}>
                    <TouchableOpacity style={styles.DeleteButton}>
                        <Text style={{color:'white',textAlign:'center'}}>
                            Delete
                        </Text>
                    </TouchableOpacity>              
                </View>
                <Text>
                        {'\n\n\n\n\n\n\n'}
                    </Text>
                <View style={{marginTop:30,alignSelf:'flex-end'}}>
                    <TouchableOpacity style={{right:10}} onPress={()=>this.props.navigation.navigate('Hub')}>
                        <HomeIcon/>    
                    </TouchableOpacity>
                </View> 
                <View>
                    <Text>
                        {'\n'}
                    </Text>
                </View>
           </View>
           <SafeAreaView style={{flex:1}}></SafeAreaView>
           </ScrollView>
           {
                    this.state.showMenu?
                    <MenuSlider navigation={this.props.navigation}/>
                        : null         
                    }  
        </>
      )
    }
}

const styles = StyleSheet.create({
    body: {
        backgroundColor: '#F85252',
        flex:1
      }, 
      scrollView: {
        backgroundColor: '#F85252',
      },
      ProfilePhoto:{
          height:250,
          width:250,
          borderRadius:150,
          backgroundColor:'white',
          elevation:50
      },
      FavouriteItemsButton:{
          height:50,
          width:160,
          backgroundColor:'#427FBC',
          padding:13,
         borderRadius:25,
         elevation:30
      },
      DonateButton:{
        height:40,
        width:80,
        backgroundColor:'#427FBC',
        padding:10,
        borderRadius:25,
        top:20,
        elevation:50,
        right:10
    },
    DeleteButton:{
        height:30,
        width:80,
        backgroundColor:'#83F852',
        padding:7,
        borderRadius:25,
        top:20,
        elevation:50
    }
})
const mapStatetoProps = (state)=>{
    return {
             account : state.account.accountType
       }
  }
export default connect(mapStatetoProps)(ProfileScreen);