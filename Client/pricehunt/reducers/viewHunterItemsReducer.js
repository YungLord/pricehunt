const InitialState = {
    SelectedItem:null
}

const ViewHunterItemsReducer = (state = InitialState, action) =>{
   
switch(action.type){
    case "ITEM": { 
        return {...state, SelectedItem: action.payload.SelectedItem}
    }
    default:{
        return state
    }
  } 
}
export default ViewHunterItemsReducer;