const initialState = {
 loginStatus:false,
 accountType:null
}

const AccountReducer = ( state = initialState , action ) => {

    switch(action.type){
        case "LOGIN": {
            return { 
                   ...state,
                   loginStatus:action.payload.status
            }      
        }
        case "HUNTER": {
            return { 
                   ...state,
                   accountType:action.payload.Hunter
            }      
        }
        case "MERCHANT": {
            return { 
                   ...state,
                   accountType:action.payload.Merchant
            }      
        }
        case "LOGOUT":{
            return{
                ...state, loginStatus:action.payload.login
            }
        }
        default:{
            return state;
        }
    }
}
export default AccountReducer;