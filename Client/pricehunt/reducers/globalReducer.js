import React from 'react';
import {combineReducers} from 'redux';
import sessionReducer from './sessionReducer';
import accountReducer from './accountReducer'
import searchReducer from './searchReducer'
import viewHunterItemsReducer from './viewHunterItemsReducer'

export const allReducers = combineReducers({
    session:sessionReducer,
    account:accountReducer,
    search:searchReducer,
    item:viewHunterItemsReducer
})
