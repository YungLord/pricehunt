"use strict";
const express = require('express');
const app = express();
require('dotenv/config');

var bodyParser = require('body-parser');
const adminService = require('./Services/AdminService');
const  favoriteService = require('./Services/FavoriteService');
const itemService = require('./Services/ItemService');
const merchantService = require('./Services/MerchantService');
const userService = require('./Services/UserService');

app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));


app.use('/user', userService);
app.use('/admin', adminService);
app.use('/favorite', favoriteService);
app.use('/item', itemService);
app.use('/merchant', merchantService);

app.use('/user', userService);
app.use('/admin', adminService);
app.use('/favorite', favoriteService);
app.use('/item', itemService);
app.use('/merchant', merchantService);

app.listen(8080, function(){
    console.log("listening on port 8080");
})

module.exports = app;
