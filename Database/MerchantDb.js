"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_merchant';
const Merchant = require('../Entities/Merchant');
const bcrypt = require('bcryptjs');

let merchants = [{
    firstName : 'Daniel',
    lastName : 'Jackson' ,
    username :  'DanOne',
    password : 'chop chop',
    companyName : 'Progressive',
    location : 'Fairview',
    email : 'progress@gmail.com',
    category : 'Supermarket'
},
{
    firstName : 'Sammuel',
    lastName : 'Jackson' ,
    username :  'Sammee',
    password : 'chomppy',
    companyName : 'Progressive',
    location : 'Fairview',
    email : 'progress@gmail.com',
    category : 'Supermarket'
},
{
    firstName : 'Daniel',
    lastName : 'Jackson' ,
    username :  'DanOne',
    password : 'chop chop',
    companyName : 'Progressive',
    location : 'Fairview',
    email : 'progress@gmail.com',
    category : 'Supermarket'
},
{
    firstName : 'Daniel',
    lastName : 'Jackson' ,
    username :  'DanOne',
    password : 'chop chop',
    companyName : 'Progressive',
    location : 'Fairview',
    email : 'progress@gmail.com',
    category : 'Supermarket'
},
{
    firstName : 'Daniel',
    lastName : 'Jackson' ,
    username :  'DanOne',
    password : 'chop chop',
    companyName : 'Progressive',
    location : 'Fairview',
    email : 'progress@gmail.com',
    category : 'Supermarket'
}
];


let merchant = {
    
        firstName : 'Daniel',
        lastName : 'Jackson' ,
        username :  'DanOne',
        password : 'chop chop',
        companyName : 'Progressive',
        location : 'Fairview',
        email : 'progress@gmail.com',
        category : 'Supermarket'
    }

//let newMerchant = new Merchant(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initMerchantDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const merchantTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Merchant Collection live");              
        });    
        return merchantTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Merchant collection')
    }
 
}
//initMerchantDb();

const addMerchant = async ( merchant )=>{   
    try{
        const merchantTable = await initMerchantDb();  
        const password = merchant.password;
        const hash = await bcrypt.hash(password,10);
        merchant.password = hash;
        const secret = process.env.accessToken;
        const token = con.jwt.sign({email: merchant.email,username : merchant.username,firstName : merchant.firstName,lastName : merchant.lastName},secret)
        const result = await merchantTable.insertOne(merchant);  
        if (result.insertedCount == 1){            
            console.log("1 Merchant added successfully");            
            return token;
        }else{
            console.log("Merchant not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Merchant not added");
        console.error(error);
    }    
}
//addMerchant(merchant);

const getMerchant = async ()=>{   
    try{
        const merchantTable = await initMerchantDb();
        const merchants = await merchantTable.find().toArray();
        console.log("Merchants retrieved!");          
        return merchants;
    }catch(error)
    {
        console.log("Database Exception: cannot get Merchants");
        console.error(error);
    }
}
// getMerchant();

const getById = async (id)=>{           
    try{            
        const merchantTable = await initMerchantDb();        
        const newMerchant = await merchantTable.findOne({_id:id});  
        if ( newMerchant != null )
            {
                console.log("merchant was found");                
                return newMerchant;
            }
            else{
                console.log("merchant was not found");
                return newMerchant;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateMerchant = async (merchant,id)=>{           
    try{        
        const merchantTable = await initMerchantDb();
        const newMerchant = await merchantTable.findOneAndUpdate(
            {_id: id},
            {$set: merchant},
            {upsert: false});

            if(newMerchant.value != null)
            {
                console.log("merchant updated");
                return 1;
            }else{
                console.log("merchant was not updated");
                return 0;
            }        
    }catch(error){
        console.log("Database Exception: merchant was not updated");
        console.error(error);
    }       
}
//updateMerchant(newMerchant,id);

const deleteMerchant = async (id)=>{               
    try{
        const merchantTable = await initMerchantDb();
        const result = await merchantTable.deleteOne({_id : id});                        
            if (result.deletedCount == 1) {
                console.log('merchant deleted');
                return 1;             
              }
            else   {              
                console.log('No merchants to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: merchant was not deleted");
        console.error(error);
    }       
}
//deleteMerchant(id);

const clearMerchant = async ()=>{
    try{        
        const merchantTable = await initMerchantDb();
        const result = await merchantTable.drop();
        console.log(result);
        
        if (result)
        {
            console.log("Merchant collection reset")
        }else{
            console.log("Merchant collection reset Failed")
        }

    }catch(error)
    {
        console.error(error);

    }
}
//clearMerchant();


const populate = async ()=>{   

    let results = [];            
    for (var i in merchants){               
       results.push(await addMerchant(merchants[i]))
    }      
    console.log("Population complete");    
}

const isDbEmpty = async() =>{
    let result = await getMerchant();    

    if (result.length == 0)
    {
        console.log('Populating Merchant Collection......');        
        populate();
    }
}

isDbEmpty();


const getByUsername = async (username)=>{    
    try{        
        const merchantTable = await initMerchantDb();
        const newMerchant = await merchantTable.findOne({username:username});         
        if ( newMerchant != null ) 
            {
                console.log("Merchant was found");            
                return newMerchant;
            }            
            else{
                console.log(" No merchan were found");
                return null;             
            }
    }catch(error){
        console.log("Database Exception: cannot get Merchant by merchantname");
        console.error(error);
    }       
}

module.exports.getById = getById;
module.exports.getMerchant = getMerchant;
module.exports.initMerchantDb = initMerchantDb;
module.exports.addMerchant = addMerchant;
module.exports.deleteMerchant = deleteMerchant;
module.exports.updateMerchant = updateMerchant;
module.exports.getByUsername = getByUsername;
