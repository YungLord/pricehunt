"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_favorite';
const Favorite = require('../Entities/Favorite');

let favorites = [{
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
},
{
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
},{
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
},
{
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
},
{
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
}


];


let favorite = {
    userId : '5ee70c749f63bf485c14e6fc',
    itemId : '5ee71e43d4353a4f1c281ee9'
}
//let newFavorite = new Favorite(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initFavoriteDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const favoriteTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Favorite Collection live");              
        });    
        return favoriteTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Favorite collection')
    }
 
}
//initFavoriteDb();

const addFavorite = async ( favorite )=>{   
    try{
        const favoriteTable = await initFavoriteDb();        
        const result = await favoriteTable.insertOne(favorite);  
        if (result.insertedCount == 1){            
            console.log("1 Favorite added successfully");            
            return result.insertedCount;
        }else{
            console.log("Favorite not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Favorite not added");
        console.error(error);
    }    
}
//addFavorite(favorite);

const getFavorite = async ()=>{   
    try{
        const favoriteTable = await initFavoriteDb();
        const favorites = await favoriteTable.find().toArray();
        console.log("Favorites retrieved!");          
        return favorites;
    }catch(error)
    {
        console.log("Database Exception: cannot get Favorites");
        console.error(error);
    }
}
// getFavorite();

const getById = async (id)=>{           
    try{            
        const favoriteTable = await initFavoriteDb();        
        const newFavorite = await favoriteTable.findOne({_id:id});  
        if ( newFavorite != null )
            {
                console.log("favorite was found");                
                return newFavorite;
            }
            else{
                console.log("favorite was not found");
                return newFavorite;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateFavorite = async (favorite,id)=>{           
    try{        
        const favoriteTable = await initFavoriteDb();
        const newFavorite = await favoriteTable.findOneAndUpdate(
            {_id: id},
            {$set: favorite},
            {upsert: false});

            if(newFavorite.value != null)
            {
                console.log("favorite updated");
                return 1;
            }else{
                console.log("favorite was not updated");
                return 0;
            }        
    }catch(error){
        console.log("Database Exception: favorite was not updated");
        console.error(error);
    }       
}
//updateFavorite(newFavorite,id);

const deleteFavorite = async (id)=>{               
    try{
        const favoriteTable = await initFavoriteDb();
        const result = await favoriteTable.deleteOne({_id : id});
                         
            if (result.deletedCount == 1) {
                console.log('favorite deleted');
                return 1;             
              }
            else   {              
                console.log('No favorites to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: favorite was not deleted");
        console.error(error);
    }       
}
//deleteFavorite(id);

const clearFavorite = async ()=>{
    try{        
        const favoriteTable = await initFavoriteDb();
        const result = await favoriteTable.drop();
        console.log(result);
        
        if (result)
        {
            console.log("Favorite collection reset")
        }else{
            console.log("Favorite collection reset Failed")
        }

    }catch(error)
    {
        console.error(error);

    }
}
//clearFavorite();


const populate = async ()=>{   

    let results = [];            
    for (var i in favorites){               
       results.push(await addFavorite(favorites[i]))
    }      
    console.log("Population complete");    
}

const isDbEmpty = async() =>{
    let result = await getFavorite();    

    if (result.length == 0)
    {
        console.log('Populating Favorite Collection......');        
        populate();
    }
}

isDbEmpty();


const getByUsername = async (username)=>{    
    try{        
        const userTable = await initFavoriteDb();
        const newUser = await userTable.findOne({username:username});         
        if ( newUser != null ) 
            {
                console.log("Favorite was found");            
                return newUser;
            }            
            else{
                console.log(" No favorite were found");
                return null;             
            }
    }catch(error){
        console.log("Database Exception: cannot get Favorite by username");
        console.error(error);
    }       
}

module.exports.getById = getById;
module.exports.getFavorite = getFavorite;
module.exports.initFavoriteDb = initFavoriteDb;
module.exports.addFavorite = addFavorite;
module.exports.deleteFavorite = deleteFavorite;
module.exports.updateFavorite = updateFavorite;
module.exports.getByUsername = getByUsername;
