"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_admin';
const Admin = require('../Entities/Admin');
const bcrypt = require('bcryptjs');

let admins = [{
    email: 'demar342@gmail.com',
    password: 'secretWord',
    firstName : 'Demar',
    lastName : 'Johnson',
    adminname : 'YungestLord'
},
{
    email: 'demir342@gmail.com',
    password: 'sefsetWord',
    firstName : 'Demario',
    lastName : 'Brown',
    adminname : 'DanDan'
},{
    email: 'Lamar342@gmail.com',
    password: 'sectWord',
    firstName : 'Lammar',
    lastName : 'Robinson',
    adminname : 'YungestLord'
},
{
    email: 'kimkay@gmail.com',
    password: 'Word',
    firstName : 'Kimone',
    lastName : 'Kaymone',
    adminname : 'KKKgirl'
},
{
    email: 'veronica@gmail.com',
    password: 'sed',
    firstName : 'Veronica',
    lastName : 'Stale',
    adminname : 'Vstale'
}


];


let admin = {
    email: 'demar342@gmail.com',
    password: 'secretWord',
    firstName : 'Demar',
    lastName : 'Johnson',
    adminname : 'YungestLord'
}
//let newAdmin = new Admin(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initAdminDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const adminTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Admin Collection live");              
        });    
        return adminTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Admin collection')
    }
 
}
//initAdminDb();

const addAdmin = async ( admin )=>{   
    try{
        const adminTable = await initAdminDb();  
        const password = admin.password;
        const hash = await bcrypt.hash(password,10);
        admin.password = hash;
        const secret = process.env.accessToken;
        const token = con.jwt.sign({email: admin.email,username : admin.username,firstName : admin.firstName,lastName : admin.lastName},secret)
        const result = await adminTable.insertOne(admin);  
        if (result.insertedCount == 1){            
            console.log("1 Admin added successfully");            
            return token;
        }else{
            console.log("Admin not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Admin not added");
        console.error(error);
    }    
}
//addAdmin(admin);

const getAdmin = async ()=>{   
    try{
        const adminTable = await initAdminDb();
        const admins = await adminTable.find().toArray();
        console.log("Admins retrieved!");          
        return admins;
    }catch(error)
    {
        console.log("Database Exception: cannot get Admins");
        console.error(error);
    }
}
// getAdmin();

const getById = async (id)=>{           
    try{            
        const adminTable = await initAdminDb();        
        const newAdmin = await adminTable.findOne({_id:id});  
        if ( newAdmin != null )
            {
                console.log("admin was found");                
                return newAdmin;
            }
            else{
                console.log("admin was not found");
                return newAdmin;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateAdmin = async (admin,id)=>{           
    try{        
        const adminTable = await initAdminDb();
        const newAdmin = await adminTable.findOneAndUpdate(
            {_id: id},
            {$set: admin},
            {upsert: false});

            if(newAdmin.value != null)
            {
                console.log("admin updated");
                return 1;
            }else{
                console.log("admin was not updated");
                return 0;
            }        
    }catch(error){
        console.log("Database Exception: admin was not updated");
        console.error(error);
    }       
}
//updateAdmin(newAdmin,id);

const deleteAdmin = async (id)=>{               
    try{
        const adminTable = await initAdminDb();
        const result = await adminTable.deleteOne({_id : id});                        
            if (result.deletedCount == 1) {
                console.log('admin deleted');
                return 1;             
              }
            else   {              
                console.log('No admins to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: admin was not deleted");
        console.error(error);
    }       
}
//deleteAdmin(id);

const clearAdmin = async ()=>{
    try{        
        const adminTable = await initAdminDb();
        const result = await adminTable.drop();
        console.log(result);
        
        if (result)
        {
            console.log("Admin collection reset")
        }else{
            console.log("Admin collection reset Failed")
        }

    }catch(error)
    {
        console.error(error);

    }
}
//clearAdmin();


const populate = async ()=>{   

    let results = [];            
    for (var i in admins){               
       results.push(await addAdmin(admins[i]))
    }      
    console.log("Population complete");    
}

const isDbEmpty = async() =>{
    let result = await getAdmin();    

    if (result.length == 0)
    {
        console.log('Populating Admin Collection......');        
        populate();
    }
}

isDbEmpty();


const getByUsername = async (username)=>{    
    try{        
        const adminTable = await initAdminDb();
        const newAdmin = await adminTable.findOne({username:username});         
        if ( newAdmin != null ) 
            {
                console.log("Admin was found");            
                return newAdmin;
            }            
            else{
                console.log(" No admin were found");
                return null;             
            }
    }catch(error){
        console.log("Database Exception: cannot get Admin by adminname");
        console.error(error);
    }       
}

module.exports.getById = getById;
module.exports.getAdmin = getAdmin;
module.exports.initAdminDb = initAdminDb;
module.exports.addAdmin = addAdmin;
module.exports.deleteAdmin = deleteAdmin;
module.exports.updateAdmin = updateAdmin;
module.exports.getByUsername = getByUsername;
