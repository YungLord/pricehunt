"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_user';
const User = require('../Entities/User');
const bcrypt = require('bcryptjs');

let users = [{
    email: 'demar342@gmail.com',
    password: 'secretWord',
    firstName : 'Demar',
    lastName : 'Johnson',
    username : 'YungestLord'
},
{
    email: 'demir342@gmail.com',
    password: 'sefsetWord',
    firstName : 'Demario',
    lastName : 'Brown',
    username : 'DanDan'
},{
    email: 'Lamar342@gmail.com',
    password: 'sectWord',
    firstName : 'Lammar',
    lastName : 'Robinson',
    username : 'YungestLord'
},
{
    email: 'kimkay@gmail.com',
    password: 'Word',
    firstName : 'Kimone',
    lastName : 'Kaymone',
    username : 'KKKgirl'
},
{
    email: 'veronica@gmail.com',
    password: 'sed',
    firstName : 'Veronica',
    lastName : 'Stale',
    username : 'Vstale'
}


];


let user = {
    email: 'demar342@gmail.com',
    password: 'secretWord',
    firstName : 'Demar',
    lastName : 'Johnson',
    username : 'YungestLord'
}
//let newUser = new User(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initUserDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const userTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("User Collection live");              
        });    
        return userTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to User collection')
    }
 
}
//initUserDb();

const addUser = async ( user )=>{   
    try{
        const userTable = await initUserDb();  
        const password = user.password;
        const hash = await bcrypt.hash(password,10);
        user.password = hash;
        const secret = process.env.accessToken;
        const token = con.jwt.sign({email: user.email,username : user.username,firstName : user.firstName,lastName : user.lastName},secret)
        const result = await userTable.insertOne(user);  
        if (result.insertedCount == 1){            
            console.log("1 User added successfully");            
            return token;
        }else{
            console.log("User not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: User not added");
        console.error(error);
    }    
}
//addUser(user);

const getUser = async ()=>{   
    try{
        const userTable = await initUserDb();
        const users = await userTable.find().toArray();
        console.log("Users retrieved!");          
        return users;
    }catch(error)
    {
        console.log("Database Exception: cannot get Users");
        console.error(error);
    }
}
// getUser();

const getById = async (id)=>{           
    try{            
        const userTable = await initUserDb();        
        const newUser = await userTable.findOne({_id:id});  
        if ( newUser != null )
            {
                console.log("user was found");                
                return newUser;
            }
            else{
                console.log("user was not found");
                return newUser;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateUser = async (user,id)=>{           
    try{        
        const userTable = await initUserDb();
        const newUser = await userTable.findOneAndUpdate(
            {_id: id},
            {$set: user},
            {upsert: false});

            if(newUser.value != null)
            {
                console.log("user updated");
                return 1;
            }else{
                console.log("user was not updated");
                return 0;
            }        
    }catch(error){
        console.log("Database Exception: user was not updated");
        console.error(error);
    }       
}
//updateUser(newUser,id);

const deleteUser = async (id)=>{               
    try{
        const userTable = await initUserDb();
        const result = await userTable.deleteOne({_id : id});                        
            if (result.deletedCount == 1) {
                console.log('user deleted');
                return 1;             
              }
            else   {              
                console.log('No users to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: user was not deleted");
        console.error(error);
    }       
}
//deleteUser(id);

const clearUser = async ()=>{
    try{        
        const userTable = await initUserDb();
        const result = await userTable.drop();
        console.log(result);
        
        if (result)
        {
            console.log("User collection reset")
        }else{
            console.log("User collection reset Failed")
        }

    }catch(error)
    {
        console.error(error);

    }
}
//clearUser();


const populate = async ()=>{   

    let results = [];            
    for (var i in users){               
       results.push(await addUser(users[i]))
    }      
    console.log("Population complete");    
}

const isDbEmpty = async() =>{
    let result = await getUser();    

    if (result.length == 0)
    {
        console.log('Populating User Collection......');        
        populate();
    }
}

isDbEmpty();


const getByUsername = async (username)=>{    
    try{        
        const userTable = await initUserDb();
        const newUser = await userTable.findOne({username:username});         
        if ( newUser != null ) 
            {
                console.log("User was found");            
                return newUser;
            }            
            else{
                console.log(" No user were found");
                return null;             
            }
    }catch(error){
        console.log("Database Exception: cannot get User by username");
        console.error(error);
    }       
}

module.exports.getById = getById;
module.exports.getUser = getUser;
module.exports.initUserDb = initUserDb;
module.exports.addUser = addUser;
module.exports.deleteUser = deleteUser;
module.exports.updateUser = updateUser;
module.exports.getByUsername = getByUsername;
