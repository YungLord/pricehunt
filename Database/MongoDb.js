"use strict";
const MongoClient = require('mongodb').MongoClient;
const path = require('path');
const con = process.env.con;
const jwt = require('jsonwebtoken'); 

        
module.exports.connectDb = async () =>{                                  
        try{        
            const db = await MongoClient.connect(con,{useUnifiedTopology: true})
            console.log('Connected to database'); 
            return db.db('PriceHunt');                            
        }catch(err){
            console.error(err);
        }    
 }   
 
 module.exports.jwt = jwt;

