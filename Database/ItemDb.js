"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_item';
const Item = require('../Entities/Item');

let items = [{
    name : 'Banana Chips',
    type : 'snack',
    price : 301.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
},
{
    name : 'Banana Chips',
    type : 'snack',
    price : 301.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
},{
    name : 'Oreo',
    type : 'snack',
    price : 35.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
},
{
    name : 'Rits Bits',
    type : 'snack',
    price : 600.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
},
{
    name : 'Cassava Chips',
    type : 'snack',
    price : 55.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
}
];


let item = {
    name : 'Plantain Chips',
    type : 'snack',
    price : 81.00,
    merchantId : '5ee301e717de155378fe93ad',
    image : 'dfjvkbncdshkbnksndsidjkbnvsi'
}
//let newItem = new Item(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initItemDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const itemTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Item Collection live");              
        });    
        return itemTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Item collection')
    }
 
}
//initItemDb();

const addItem = async ( item )=>{   
    try{
        const itemTable = await initItemDb();        
        const result = await itemTable.insertOne(item);  
        if (result.insertedCount == 1){            
            console.log("1 Item added successfully");            
            return result.insertedCount;
        }else{
            console.log("Item not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Item not added");
        console.error(error);
    }    
}
//addItem(item);

const getItem = async ()=>{   
    try{
        const itemTable = await initItemDb();
        const items = await itemTable.find().toArray();
        console.log("Items retrieved!");          
        return items;
    }catch(error)
    {
        console.log("Database Exception: cannot get Items");
        console.error(error);
    }
}
// getItem();

const getById = async (id)=>{           
    try{            
        const itemTable = await initItemDb();        
        const newItem = await itemTable.findOne({_id:id});  
        if ( newItem != null )
            {
                console.log("item was found");                
                return newItem;
            }
            else{
                console.log("item was not found");
                return newItem;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateItem = async (item,id)=>{           
    try{        
        const itemTable = await initItemDb();
        const newItem = await itemTable.findOneAndUpdate(
            {_id: id},
            {$set: item},
            {upsert: false});

            if(newItem.value != null)
            {
                console.log("item updated");
                return 1;
            }else{
                console.log("item was not updated");
                return 0;
            }        
    }catch(error){
        console.log("Database Exception: item was not updated");
        console.error(error);
    }       
}
//updateItem(newItem,id);

const deleteItem = async (id)=>{               
    try{
        const itemTable = await initItemDb();
        const result = await itemTable.deleteOne({_id : id});                        
            if (result.deletedCount == 1) {
                console.log('item deleted');
                return 1;             
              }
            else   {              
                console.log('No items to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: item was not deleted");
        console.error(error);
    }       
}
//deleteItem(id);

const clearItem = async ()=>{
    try{        
        const itemTable = await initItemDb();
        const result = await itemTable.drop();
        console.log(result);
        
        if (result)
        {
            console.log("Item collection reset")
        }else{
            console.log("Item collection reset Failed")
        }

    }catch(error)
    {
        console.error(error);

    }
}
//clearItem();


const populate = async ()=>{   

    let results = [];            
    for (var i in items){               
       results.push(await addItem(items[i]))
    }      
    console.log("Population complete");    
}

const isDbEmpty = async() =>{
    let result = await getItem();    

    if (result.length == 0)
    {
        console.log('Populating Item Collection......');        
        populate();
    }
}

isDbEmpty();


const getByUsername = async (username)=>{    
    try{        
        const userTable = await initItemDb();
        const newUser = await userTable.findOne({username:username});         
        if ( newUser != null ) 
            {
                console.log("Item was found");            
                return newUser;
            }            
            else{
                console.log(" No item were found");
                return null;             
            }
    }catch(error){
        console.log("Database Exception: cannot get Item by username");
        console.error(error);
    }       
}

module.exports.getById = getById;
module.exports.getItem = getItem;
module.exports.initItemDb = initItemDb;
module.exports.addItem = addItem;
module.exports.deleteItem = deleteItem;
module.exports.updateItem = updateItem;
module.exports.getByUsername = getByUsername;
