
"use strict";
const mongoose = require('mongoose');


const FavoriteSchema = new mongoose.Schema({
    userId : String,
    itemId : String
});

mongoose.model('Favorite', FavoriteSchema);
module.exports = mongoose.model('Favorite');

