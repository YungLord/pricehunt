"use strict";
const mongoose = require('mongoose');


const AdminSchema = new mongoose.Schema({
    email: String,
    password: String,
    firstName : String,
    lastName : String,
    username : String
});

mongoose.model('Admin', AdminSchema);
module.exports = mongoose.model('Admin');

