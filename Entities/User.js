"use strict";
const mongoose = require('mongoose');


const UserSchema = new mongoose.Schema({
    email: String,
    password: String,
    firstName : String,
    lastName : String,
    username : String
});

mongoose.model('User', UserSchema);
module.exports = mongoose.model('User');

