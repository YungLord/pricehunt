
"use strict";
const mongoose = require('mongoose');


const ItemSchema = new mongoose.Schema({
    name : String,
    type : String,
    price : Number,
    merchantId : String,
    image : String    
});

mongoose.model('Item', ItemSchema);
module.exports = mongoose.model('Item');

