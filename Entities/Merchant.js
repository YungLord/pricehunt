
"use strict";
const mongoose = require('mongoose');


const MerchantSchema= new mongoose.Schema({
    firstName : String,
    lastName : String,
    username : String,
    password : String,
    companyName : String,
    location : String,            
    email : String,
    category : String
});

mongoose.model('Merchant',MerchantSchema);
module.exports = mongoose.model('Merchant');
